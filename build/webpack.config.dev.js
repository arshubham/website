const baseConfig = require('./webpack.config.base'),
      merge = require('webpack-merge'),
      webpack = require('webpack');

// eslint-disable-next-line one-var
const HOST = '127.0.0.1',
      PORT = 8080;


module.exports = merge(baseConfig, {
  devServer: {
    clientLogLevel: 'info',
    compress: true,
    contentBase: 'dist',
    host: HOST,
    hot: true,
    open: true,
    overlay: {
      errors: true,
      warnings: false,
    },
    port: PORT,
    publicPath: '/',
    stats: 'normal',
    watchOptions: {
      poll: true
    },
  },
  devtool: 'inline-source-map',
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.css$/u,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.scss$/u,
        use:
        [
          'vue-style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
    ],
  },
  plugins:
  [
    new webpack.EnvironmentPlugin({
      DEBUG: true,
      NODE_ENV: 'development',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
});